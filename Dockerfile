FROM centos:7

ARG USERNAME=fakeuser
ARG PASSWORD=secretpassword

# Installing updates required for Spack
RUN yum update -y && \
    yum install -y epel-release && \
    yum update -y

RUN yum --enablerepo epel groupinstall -y "Development Tools" && \
    yum --enablerepo epel -y install python3 boost-devel root root-guibuilder libSM libICE libX11 libXext libXpm libXft

# Installing additional components for use by Spack as external packages
RUN git clone https://${USERNAME}:${PASSWORD}@gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git /opt/itsdaq-sw && \
    cd /opt/itsdaq-sw && \
    python3 waf configure build install